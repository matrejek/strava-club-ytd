#!/usr/bin/python3

from lxml import html
import requests
import hashlib
import sys
import re
import datetime

netlifyApi = 'https://api.netlify.com/api/v1/'


def profileUrl(userId):
    return "https://www.strava.com/athletes/" + userId


def profileLinkHTML(profileUrl):
    return "<a href=\"" + profileUrl + "\">Strava profile</a>"


def clubMembersUrl(clubId, page):
    return 'https://www.strava.com/api/v3/clubs/' + \
        clubId + '/members?page=' + str(pageIndex)


def clubUrl(clubId):
    return 'https://www.strava.com/api/v3/clubs/' + clubId


def achievementsPresent(tree):
    return len(tree.xpath('//*[@id="overview"]/section[2]/h2/text()')) > 0


def tableRow(c1, c2, c3, c4):
    return ('<tr><td>' + c1 + '</td><td>' +
            c2 + '</td><td>' +
            c3 + "</td><td>" +
            c4 + "</td></tr>")


def summaryText(tree, section, row):
    return tree.xpath('//*[@id="overview"]/section[' +
                      str(section) + ']/div[1]/table/tbody/tr[' +
                      str(row) + ']/td/text()')[0]


def validateStatisticsHeader(achievementsPresent):
    offset = 0
    if achievementsPresent:
        offset = 1
    xpath = '//*[@id="overview"]/section[' + \
        str(2 + offset) + ']/div[1]/h2/text()'
    content = tree.xpath(xpath)
    if len(content) > 0:
        return content[0] == 'Year-to-Date'
    return False


def sanitize(input):
    return re.sub("<.*?>", "", input)


# process params

if not len(sys.argv) == 5:
    print("script.py strava_token club_id netlify_token site_id")
    exit(-1)

stravaToken = sys.argv[1]
CLUB_ID = sys.argv[2]
SITE_ID = sys.argv[4]
NETLIFY_TOKEN = sys.argv[3]

stravaHeaders = {'Authorization': 'Bearer ' + stravaToken}

# get club info

r = requests.get(clubUrl(CLUB_ID), headers=stravaHeaders)
if not r.status_code == 200:
    print("Error getting club info: " + str(r.status_code))
    exit(-1)
clubJson = r.json()
profilePhotoUrl = clubJson['profile']
clubName = clubJson['name']
clubLink = 'https://www.strava.com/clubs/' + clubJson['url']

# get club members

downloadedUsers = []
isEmpty = False
pageIndex = 1
while not isEmpty:
    url = clubMembersUrl(CLUB_ID, pageIndex)
    r = requests.get(url, headers=stravaHeaders)
    if r.status_code == 200:
        downloadedUsers.extend(r.json())
        isEmpty = len(r.json()) == 0
        pageIndex += 1
    else:
        print("Error getting users: " + str(r.status_code))
        exit(-1)

print('Downloaded users: ' + str(len(downloadedUsers)))

rank = []
distanceSum = 0.0
elevationSum = 0.0
ridesSum = 0

for i, val in enumerate(downloadedUsers):
    url = profileUrl(str(val["id"]))
    page = requests.get(url)
    tree = html.fromstring(page.content)
    distance = ""
    rides = ""
    elevation = ""
    if achievementsPresent(tree):
        if not validateStatisticsHeader(True):
            print("Skipping: " + str(val["id"]))
            continue
        distance = float(summaryText(tree, 3, 1).replace(",", ""))
        rides = int(summaryText(tree, 3, 4).replace(",", ""))
        elevation = float(summaryText(tree, 3, 3).replace(",", ""))
    else:
        if not validateStatisticsHeader(False):
            print("Skipping: " + str(val["id"]))
            continue
        distance = float(summaryText(tree, 2, 1).replace(",", ""))
        rides = int(summaryText(tree, 2, 4).replace(",", ""))
        elevation = float(summaryText(tree, 2, 3).replace(",", ""))
    userName = sanitize(str(val["firstname"]) + " " + str(val["lastname"]))
    user = {"name": userName,
            "dist": distance,
            "up": elevation,
            "rides": rides,
            "id": str(val["id"])}
    distanceSum += distance
    elevationSum += elevation
    ridesSum += rides
    rank.append(user)
    print("Done: " + user["id"] + " " + str(i + 1) +
          "/" + str(len(downloadedUsers)))

with open('index_template.html', 'r') as template:
    content = template.read()

# generate rank tables

distanceHTML = ""
elevationHTML = ""
ridesHTML = ""

for i, user in enumerate(sorted(rank, key=lambda k: k['dist'], reverse=True)):
    distanceHTML += tableRow(str(i + 1),
                             user["name"],
                             str(user["dist"]) + ' km',
                             profileLinkHTML(profileUrl(str(user["id"]))))
for i, user in enumerate(sorted(rank, key=lambda k: k['up'], reverse=True)):
    elevationHTML += tableRow(str(i + 1),
                              user["name"],
                              str(user["up"]) + ' m',
                              profileLinkHTML(profileUrl(str(user["id"]))))
for i, user in enumerate(sorted(rank, key=lambda k: k['rides'], reverse=True)):
    ridesHTML += tableRow(str(i + 1),
                          user["name"],
                          str(user["rides"]),
                          profileLinkHTML(profileUrl(str(user["id"]))))

# generate data timestamp

timestamp = datetime.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S") + ' UTC'

# apply changes in template html

content = content.replace("$timestamp", timestamp)
content = content.replace("$club_name", clubName)
content = content.replace("$club_url", clubLink)
content = content.replace("$club_logo", profilePhotoUrl)
content = content.replace("$dist_rank", distanceHTML)
content = content.replace("$elevation_rank", elevationHTML)
content = content.replace("$rides_rank", ridesHTML)
content = content.replace(
    "$total_dist", "{:,}".format(round(distanceSum)) + ' km')
content = content.replace("$total_elev", "{:,}".format(
    round(elevationSum / 1000)) + ' km')
content = content.replace("$total_rides", str(ridesSum))


sha1 = hashlib.sha1(content.encode()).hexdigest()

updateURL = netlifyApi + 'sites/' + \
    SITE_ID + "/deploys?access_token=" + NETLIFY_TOKEN
r = requests.post(updateURL, json={
                  "files": {"/index.html": sha1}},
                  headers={'Content-Type': 'application/json'})
deployId = r.json()['id']
fileUrl = netlifyApi + 'deploys/' + \
    deployId + '/files/index.html?access_token=' + NETLIFY_TOKEN
r = requests.put(fileUrl, headers={
                 'Content-Type': 'application/octet-stream'},
                 data=content.encode('utf-8'))
if r.status_code != 200:
    print("Error uploading data: " + str(r.status_code))
    exit(-1)
